<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use App\Models\User;
use Illuminate\Queue\RedisQueue;
use Symfony\Component\HttpFoundation\RateLimiter\RequestRateLimiterInterface;
use Dotenv\Validator;
use Illuminate\Support\Facades\Session;

use function PHPUnit\Framework\returnSelf;

class LoginController extends Controller
{
    public function index(){
                return view('login', [
                    'title' => 'Login'
                ]);
            }

            public function authenticate(Request $request, User $user){
                    $credentials = [
                        'username' => $request->input('username'),
                        'password' => $request->input('password'),
                    ];
            
                    // dd(auth::attempt($credentials));
                    if (Auth::Attempt($credentials)) {
                        $request->session()->regenerate();
                        
                    if((auth()->user()->role == 'pelayan')){
                        return redirect()->intended('/dashboard');
                    }elseif(auth()->user()->role == 'kasir'){
                        return redirect()->intended('/dashboard/meja/create');
                    }
                        
                    }
                    return back()->with('loginError', 'Gagal Login, Data User Tidak Ditemukan');
                }

        public function logout(Request $request){
                Auth::logout();
                
                request()->session()->invalidate();

                request()->session()->regenerateToken();

                return redirect('/login');
                }

        public function register(){
                return view('register', [
                    'title' => 'register'
                ]);
            }

            public function store(Request $request){
                $rules = [
                    'nip' => 'required',
                    'name' => 'required',
                    'role' => 'required',
                    'username' => 'required',
                    'password' => 'required',
                ];
                
                $validateData = $request->validate($rules);
                
                User::create($validateData);
                // dd($request);

                return redirect('/login')->with('success', 'Registrasi Anda Berhasil, Anda Dapat Melakukan Login Sekarang!');
            }    
}
