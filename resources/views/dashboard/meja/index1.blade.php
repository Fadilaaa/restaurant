@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight text-dark">Pesan Menu</h6>
        </div>
        <div class="row-border">
            <div class="col-lg-12">
                <div class="card-body">
                    <div class="dashboard">
                        <div class="container-fluid">             
                <div class="app-card-body px-4 w-100 my-3">
                    <form action="/dashboard/meja" class="mt-4">
                        @csrf
                        <div class="input-group mb-3">
                            <select class="form-select" name="namameja" id="" style="max-width: 200px">
                                @if(request('namameja'))
                                <option value="{{ request('namameja') }}" selected hidden>meja {{ request('namameja') }}</option>
                                <option value="namameja1">Meja 1</option>
                                <option value="namameja2">Meja 2</option>
                                <option value="namameja3">Meja 3</option>
                                <option value="namameja4">Meja 4</option>
                                <option value="namameja5">Meja 5</option>
                                <option value="namameja6">Meja 6</option>
                                @else
                                <option value="namameja1">Meja 1</option>
                                <option value="namameja2">Meja 2</option>
                                <option value="namameja3">Meja 3</option>
                                <option value="namameja4">Meja 4</option>
                                <option value="namameja5">Meja 5</option>
                                <option value="namameja6">Meja 6</option>
                                @endif
                            </select>
                            <button class="btn btn-info"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                    @if(!$_GET)
                    @else
                        @if($_GET['namameja'] == 1)
                        <div class="alert alert-primary col-lg-12" role="alert">
                        Meja 1
                        </div>
                        @elseif($_GET['namameja'] == 2)
                        <div class="alert alert-primary col-lg-12" role="alert">
                        Meja 2
                        </div>
                        @elseif($_GET['namameja'] == 3)
                        <div class="alert alert-primary col-lg-12" role="alert">
                        Meja 3
                        </div>
                        @elseif($_GET['namameja'] == 4)
                        <div class="alert alert-primary col-lg-12" role="alert">
                        Meja 4
                        </div>
                        @elseif($_GET['namameja'] == 5)
                        <div class="alert alert-primary col-lg-12" role="alert">
                        Meja 5
                        </div>
                        @elseif($_GET['namameja'] == 6)
                        <div class="alert alert-primary col-lg-12" role="alert">
                        Meja 6
                        </div>
                        @endif
                    @endif
                    <form action="/dashboard/meja">
                        @foreach($listmenu as $menu)
                        <!-- Form -->
                        <div class="form-group mb-4">
                            <div class="input-group">
                                  <div class="card">
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if($menu->gambar)
                                            <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-sm-12 d-block">
                                        @else
                                            <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                        @endif
                                        </div>
                                        <div class="col-md-8">
                                            <h4 class="card-title mt-2">{{ $menu->namamenu }}</h4>
                                            <p>{{ $menu->deskripsi }}</p>
                                            <div class="display d-flex">
                                                <p>{{ $menu->harga }}</p>
                                                <input type="number" name="meja1" id="meja1" class="col-lg-4">
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                            </div>  
                        </div>
                        @endforeach
                        <!-- End of Form -->
                        <div class="d-grid">
                            <button type="submit" class="btn btn-warning">Pesan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>    
    </div>
</div>
</div>
</div>
<script>
    function previewImage(){
        const gambar = document.querySelector('#gambar');
        const imgPreview = document.querySelector('.img-preview1');

        imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(gambar.files[0]);

        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
@endsection