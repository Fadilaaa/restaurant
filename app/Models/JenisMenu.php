<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisMenu extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function menu(){
        return $this->hasMany(ListMenu::class);
    }

    public function listmeja(){
        return $this->hasMany(ListMeja::class);
    }

    public function order(){
        return $this->hasMany(Order::class);
    }

    public function user(){
        return $this->hasMany(User::class);
    }

    public function orderdetails(){
        return $this->hasMany(OrderDetails::class, 'id', 'id_order');
    }
}
