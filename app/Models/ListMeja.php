<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListMeja extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function menu(){
        return $this->hasMany(ListMenu::class);
    }

    public function order(){
        return $this->hasMany(Order::class, 'id', 'id_meja');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function jenismenu(){
        return $this->hasMany(JenisMenu::class);
    }

    public function orderdetails(){
        return $this->belongsTo(OrderDetails::class, 'id', 'id_order');
    }
}
