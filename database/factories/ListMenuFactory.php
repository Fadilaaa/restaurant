<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ListMenu>
 */
class ListMenuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'kodemenu' => $this->faker->numberBetween($min=2, $max=3),
            'namamenu' => $this->faker->sentence(1,2),
            'jenismenu' => $this->faker->numberBetween($min=1, $max=2),
            'deskripsi' => $this->faker->sentence(1,2),
            'harga' => $this->faker->numberBetween($min=3, $max=4),
            'gambar' => $this->faker->fileExtension(),
        ];
    }
}
