<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            // 'user_id' => $this->faker->sentence(),
            // 'kodepesanan' => $this->faker->numberBetween($min=2, $max=3),
            'kodeorder' => $this->faker->numberBetween($min=1, $mas=2),
            'id_meja' => $this->faker->numberBetween($min=1, $mas=2),
            'atasnama' => $this->faker->name(),
            'status' => $this->faker->numberBetween($min=1, $max=2),
        ];
    }
}
