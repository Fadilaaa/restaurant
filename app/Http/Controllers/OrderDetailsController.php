<?php

namespace App\Http\Controllers;

use App\Models\OrderDetails;
use App\Http\Requests\StoreOrderDetailsRequest;
use App\Http\Requests\UpdateOrderDetailsRequest;
use App\Models\ListMenu;
use App\Models\ListMeja;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListMenu $listmenu, Request $request)
    {
        $listmeja = ListMeja::where('status', 0)->get();

        if(($request->meja) == 0){
            $listmeja = ListMeja::where('status', 0)->get();
        }
        elseif(($request->meja) == 1){
            $listmeja = ListMeja::where('status', 1)->get(); 
        }

            $listmenu = ListMenu::get();
            $order = Order::get();
        return view('meja',[
            'listmenu' => $listmenu,
            'listmeja' => $listmeja,
            'order' => $order,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderDetailsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderDetailsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function show(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderDetailsRequest  $request
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderDetailsRequest $request, OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderDetails $orderDetails)
    {
        //
    }
}
