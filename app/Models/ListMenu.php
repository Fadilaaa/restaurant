<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListMenu extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function order(){
        return $this->hasMany(Order::class, 'id', 'id_menu');
    }

    public function listmeja(){
        return $this->hasMany(ListMeja::class);
    }

    public function user(){
        return $this->hasMany(User::class);
    }

    public function jenismenu(){
        return $this->belongsTo(JenisMenu::class, 'id');
    }

    public function orderdetails(){
        return $this->hasMany(OrderDetails::class, 'id', 'id_order');
    }
}
