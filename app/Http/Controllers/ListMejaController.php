<?php

namespace App\Http\Controllers;

use App\Models\ListMeja;
use App\Models\ListMenu;
use App\Http\Requests\StoreListMejaRequest;
use App\Http\Requests\UpdateListMejaRequest;
use App\Models\Order;
use App\Models\OrderDetails;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\List_;

class ListMejaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListMenu $listmenu, Order $order)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ListMenu $listmenu, Request $request)
    {
        // dd($order->id_meja);
        // $sudah_memesan = [];
        // $belum_memesan = [];
        // $sudah_memesan = ListMeja::where('status', 1)->get()[0];
        // $belum_memesan = ListMeja::where('status', 0)->get()[0];
        // if($sudah_memesan == '1'){
        //     // dd($belum_memesan);
        //     // if($sudah_memesan == '1'){
        //     echo "<script>
        //             alert('Meja Ini Sudah Dipesan');
        //             document.location.href='/dashboard/meja/create'
        //         </script>";
        //     }

        $listmeja = ListMeja::where('status', 0)->get();

        if(($request->meja) == 0){
            $listmeja = ListMeja::where('status', 0)->get();
        }
        elseif(($request->meja) == 1){
            $listmeja = ListMeja::where('status', 1)->get(); 
        }
        // }elseif(count([$sudah_memesan]) == 0){
            // dd($sudah_memesan);
            // if($sudah_memesan == '1'){
        

            $listmenu = ListMenu::get();
            $order = Order::get();
            // $listmeja = ListMeja::get();
          return view('dashboard.meja.create',[
            'listmenu' => $listmenu,
            'listmeja' => $listmeja,
            'order' => $order,
            // 'sudah_memesan' => count([$sudah_memesan]),
            // 'belum_memesan' => count([$belum_memesan]),
        ]);
        
       }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreListMejaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order, ListMeja $listmeja, OrderDetails $orderdetails)
    {
        $listmeja = ListMeja::with(['listmenu', 'order']);

        // foreach($request->id_menu as $row){
        //     $jawabanbenar = OrderDetails::where('id', $row)->get();
        // }
        // $add = [
        //     'id_meja' => $request->id_meja,
        //     'atasnama' => $request->atasnama,
        // ]
        $code = 'KOR-' . mt_rand(000000, 999999);

        $pesan = Order::create([
            // 'kodepesanan' => $order->kodepesanan,
            'kodeorder' => $code,
            'id_meja' => $request->id_meja,
            'atasnama' => $request->atasnama,
            'status' => 1,
        ]);

        // $request->id_meja = ListMeja::create([
        //     'status' => 1
        // ]);

        ListMeja::where('id', $request->id_meja)
            ->update([
            'status' => 1
        ]);
        // dd($request);
        $count = count($request->id_menu);

        for ($i=0; $i < $count; $i++) { 
            $od = new OrderDetails();
            $od->id_order = $pesan->id;
            $od->id_meja = $request->id_meja;
            $od->id_menu = $request->id_menu[$i];
            $od->namamenu = $request->namamenu[$i];
            $od->harga = $request->harga[$i];
            $od->qty = $request->qty[$i];
            $od->total = $request->harga[$i] * $request->qty[$i];
            $od->save();
        }

        // dd($listmeja->id);

            return redirect('/dashboard/meja/create')->with('success', 'Pesanan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListMeja  $listMeja
     * @return \Illuminate\Http\Response
     */
    public function show(ListMeja $listMeja)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListMeja  $listMeja
     * @return \Illuminate\Http\Response
     */
    public function edit(ListMenu $listMenu, Order $order)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateListMejaRequest  $request
     * @param  \App\Models\ListMeja  $listMeja
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateListMejaRequest $request, ListMeja $listMeja)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListMeja  $listMeja
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListMeja $listMeja)
    {
        //
    }

    public function index01(ListMeja $listMeja){
        return view('dashboard.modal.meja1', [
            'title' => 'meja1',
            'listmeja' => $listMeja
        ]);
    }
}
