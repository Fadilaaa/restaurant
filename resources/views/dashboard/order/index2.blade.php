@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">
    <div class="mb-3 mb-lg-0">
        <h1 class="h4">Order</h1>
    </div>
    <!-- Page Heading -->
    {{-- <div class="card shadow mb-4"> --}}
      
        <div class="card">
        <div class="card-body">
            <div class="dashboard">
                <div class="container-fluid">
                    @if(session()->has('success'))
              <div class="alert alert-success col-lg-8" role="alert">
                {{ session('success') }}
              </div>
              @endif
            <form action="/dashboard/order" method="get" name="orderan">
                @csrf
            <div class="input-group mb-3">
                <select name="orderan" id="" class="form-select" style="max-width: 200px">
                    @if(request('orderan'))
                    <option value="{{ request('orderan') }}" select hidden>Status Pesanan</option>
                        <option value="1">Sedang Mengorder</option>
                        <option value="2">Sudah Mengorder</option>
                        @else
                        <option value="1">Sedang Mengorder</option>
                        <option value="2">Sudah Mengorder</option>
                    @endif
                </select>
                <button class="btn btn-info"><i class="bi bi-search"></i></button>
            </div>
        </form>
        @if(!$_GET)
        @else
            @if($_GET['orderan'] == 1)
            <div class="alert alert-success col-lg-12">Menampilkan Pesanan Yang Sedang Diorder</div>
            @elseif($_GET['orderan'] == 2)
            <div class="alert alert-success col-lg-12">Menampilkan Pesanan Yang Sudah Mengorder</div>
            @endif
        @endif
        <br>
        @if(request('orderan') == 1)    
        <table id="example" class="table table-centered table-nowrap mb-0 rounded " style="width=100%">
                <thead class="text-center text-dark">
                    <tr class="text-center">
                        <th scope="col">#</th>
                        <th scope="col">Meja</th>
                        <th scope="col">Atas Nama</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                {{-- {{ $order }} --}}
                @foreach ($order as $o)
                <tr class="text-center">
                    <td>{{ $loop->iteration }}</td> 
                    <td>{{ $o->id_meja }}</td>
                    <td>{{ $o->atasnama }}</td>
                    <td>
                    <div class="d-flex justify-content-center">    
                        <div class="col-12">    
                            <form action="/dashboard/order" method="get" name="orderan">
                                @csrf
                            <div class="input-group mb-3 align-text-center">
                                {{-- Button View Order --}}
                                <a href="/dashboard/order/show/{{ $o->id }}" class="btn btn-info" value={{ $o->id }}>
                                    <i class="bi bi-eye"></i>
                                </a>
                                <!-- Button Modal Tambah-->
                                <button type="button" class="btn btn-primary" id="{{ $o->id_meja }}" name="{{ $o->id_meja }}" value="{{ $o->id }}" data-bs-toggle="modal" data-bs-target="#modal-form-signup{{ $o->id }}">
                                    <i class="bi bi-bag-plus"></i>
                                </button>
                                <h5 class="text-center">{{ $o->namameja }}</h5>                           
                                {{-- Button Close Order --}}
                                <form action="/dashboard/order/{{ $o->id }}/close" method="post" class="d-inline">
                                    {{-- {{ $o->id }} --}}
                                    @method('put')
                                    {{-- @csrf --}}
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Akan Menutup Pesanan Ini?')"><i class="bi bi-x"></i></button>
                                </form>
                            </div>
                        </form>
                       
                        </div>
                </div>
                    </td>
                @endforeach 
            </tbody>
            </table>
            @elseif(request('orderan') == 2)
            <table id="example" class="table table-centered table-nowrap mb-0 rounded " style="width=100%">
                <thead class="text-center text-dark">
                    <tr class="text-center">
                        <th scope="col">#</th>
                        <th scope="col">Meja</th>
                        <th scope="col">Atas Nama</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                {{-- {{ $order }} --}}
                @foreach ($order as $o)
                <tr class="text-center">
                    <td>{{ $loop->iteration }}</td> 
                    <td>{{ $o->id_meja }}</td>
                    <td>{{ $o->atasnama }}</td>
                    <td>
                    <div class="d-flex justify-content-center">    
                        <div class="col-12">    
                            <div class="input-group mb-3 align-text-center">
                                        {{-- Button View Order --}}
                                        <a href="/dashboard/order/show/{{ $o->id }}" class="btn btn-info" value={{ $o->id }}>
                                            <i class="bi bi-eye"></i>
                                        </a>
                            </div>
                        </div>
                    </div>
                    </td>
                @endforeach 
            </tbody>
            </table>
            @endif
        </div>
        </div>
    </div>
</div>
<!-- Modal Content -->
<input type="hidden" id="{{ $o->id_meja }}" name="{{ $o->id_meja }}" value="{{ $o->id_meja }}">
<div class="modal fade" id="modal-form-signup{{ $o->id }}" name="{{ $o->id_meja }}" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup{{ $o->id }}" aria-hidden="true" value="{{ $o->id_meja }}">
    <div class="modal-dialog modal-dialog-centered" role="document" value={{ $o->id_meja }}>
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="card p-3 p-lg-4">
                    <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center text-md-center mb-4 mt-md-0">
                        <h1 class="mb-0 h4">Pesan Menu</h1>
                    </div>
                    <form action="/dashboard/order/{{ $o->id }}" class="mt-4" method="post" id="{{ $o->id }}" value="{{ $o->id_meja }}" name="{{ $o->id_meja }}">
                        {{-- @method('put') --}}
                        @csrf
                        <input type="text" class="form-control" name="id" value="{{ $o->id }}">
                        <input type="text" class="form-control" name="id_meja" value="{{ $o->id_meja }}">
                        @foreach($listmenu as $menu)
                        {{-- @var_dump($menu->id) --}}
                        <!-- Form -->
                        <div class="form-group mb-4">
                            <div class="input-group">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-6">
                                            {{-- <input type="hidden" value="{{ $menu->id_menu }}"> --}}
                                            @if($menu->gambar)
                                            <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-12" style='height: 100%; width: 100%; object-fit: contain'>
                                            @else
                                            <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                            @endif
                                        </div>
                                        <div class="col-5">
                                                <h4 class="card-title mt-2" name="namamenu[]" value="{{ $menu->namamenu }}">{{ $menu->namamenu }}</h4>
                                                <label for="deskripsi" class="form-label" value="{{ $menu->deskripsi }}">{{ $menu->deskripsi }}</label>
                                            <br>
                                            {{-- <label for="harga" class="form-label">{{ $menu->harga }}</label> --}}
                                            <div class="mb-3 col-lg-12">
                                                <input type="hidden" name="id_menu[]" class="col-11" value="{{ $menu->id }}">
                                                <input type="hidden" name="namamenu[]" class="form-control @error('namamenu') is-invalid @enderror col-10" value="{{ $menu->namamenu }}">
                                                <input type="text" name="harga[]" class="form-control @error('harga') is-invalid @enderror col-10" value="{{ $menu->harga }}">
                                                <br>
                                                <input type="number" min="0" class="form-control @error('qty') is-invalid @enderror col-10" id="qty[]" name="qty[]" value="0">
                                                @error('qty')
                                                <div class="invalid-feedback">
                                                {{ $message }}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        @endforeach
                        <!-- End of Form -->
                        <div class="d-grid">
                            <button type="submit" class="btn btn-warning">Tambah</button>
                        </div>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection