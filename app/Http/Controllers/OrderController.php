<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\JenisMenu;
use App\Models\ListMeja;
use App\Models\ListMenu;
use App\Models\OrderDetails;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = Order::where('status', 1)->get();

        if(($request->orderan) == 1){
            $order = Order::where('status', 1)->get();
        }
        elseif(($request->orderan) == 2){
            $order = Order::where('status', 2)->get(); 
        }

        // dd($request->orderan);
        $listmenu = ListMenu::get();
        // $order = Order::get();
        $listmeja = ListMeja::get();
        return view('dashboard.order.index', [
            'listmenu' => $listmenu,
            'listmeja' => $listmeja,
            'order' => $order,
        ]);
    }

    public function close($id, ListMeja $listmeja, Request $request){
        $order = Order::where('id', $id)
        ->get()[0];
        
        Order::where('id', $order->id)
        ->update([
            'status' => 2
        ]);

        $listmeja = Order::where('id', $id)
        ->get()[0];
        
        ListMeja::where('id', $listmeja->id_meja)
        ->update([
            'status' => 0
        ]);
        // dd($listmeja->id_meja);

        return redirect('/dashboard/order')->with('success', 'Anda Telah Menutup Pesanan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ListMeja $listmeja)
    {
     
    //     $listmenu = ListMenu::get();
    //     $order = Order::get();
    //     $listmeja = ListMeja::get();
    //   return view('dashboard.order.index',[
    //     'listmenu' => $listmenu,
    //     'listmeja' => $listmeja,
    //     'order' => $order
    // ]);

        // $orderdetails = OrderDetails::get();
        // return view('dashboard.order.index', [
        //     'title' => 'createorder'
        // ]);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
       
        

   
        // return redirect('/dashboard/order')->with('success', 'Pesanan Berhasil Ditambahkan');   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order, ListMenu $listmenu, OrderDetails $orderdetails, ListMeja $listmeja, $id)
    {
        // $listmenu = ListMenu::get();
        $orderdetails = OrderDetails::where('id_order', $id)->get();

        $total = collect($orderdetails)->sum('total');

        $order = Order::where('id', $id)->get()[0];
        // dd($order);
        $listmeja = ListMeja::get();
        return view('dashboard.order.show', [
            'listmenu' => $listmenu,
            'orderdetails' => $orderdetails,
            'listmeja' => $listmeja,
            'order' => $order,
            'title' => 'Order',
            'total' => $total
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
    //     $listmenu = ListMenu::get();
    //     $order = Order::get();
    //     $listmeja = ListMeja::get();
    //   return view('dashboard.order.edit',[
    //     'listmenu' => $listmenu,
    //     'listmeja' => $listmeja,
    //     'order' => $order
    // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $listmeja = ListMeja::with(['listmenu', 'order']);
        
      
        $mesen = Order::where('id', $order->id)->get()[0];
            // dd($request);
            
        $count = count($request->id_menu);
        
        for ($i=0; $i < $count; $i++) { 
            $od = new OrderDetails();
            $od->id_order = $mesen->id;
            $od->id_meja = $request->id_meja;
            $od->id_menu = $request->id_menu[$i];
            $od->namamenu = $request->namamenu[$i];
            $od->harga = $request->harga[$i];
            $od->qty = $request->qty[$i];
            $od->total = $request->harga[$i] * $request->qty[$i];
            $od->save();
        }

        return redirect('/dashboard/order')->with('success', 'Menu Berhasil Ditambahkan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        Order::destroy($order->id);
        return redirect('/dashboard/menu')->with('success', 'Order Berhasil Dihapus');
    }

    public function editorder(Order $order, $id){

        $orderdetails = OrderDetails::where('id_order', $id)->get();

        $total = collect($orderdetails)->sum('total');

        $order = Order::where('id', $id)->get()[0];
        $listmeja = ListMeja::get();
        $listmenu = ListMenu::get();
        // dd($orderdetails);
        // dd($order);
        return view('dashboard.order.edit', [
            'listmenu' => $listmenu,
            'orderdetails' => $orderdetails,
            'listmeja' => $listmeja,
            'order' => $order,
            'title' => 'Order',
            'total' => $total
        ]);
        // dd($order);
    }

    public function updateorder(Order $order, Request $request, OrderDetails $orderdetails){
        $listmeja = ListMeja::with(['listmenu', 'order']);
        
        
        // dd($request->id);
        $mesen = Order::where('id', $request->id)->get()[0];
        
        // $orderan = OrderDetails::where('id_order', $orderdetails->id)->get()[0];
        
        // dd($request->order);
        $count = count($request->id_menu);

        
        // $od = OrderDetails::where('id', $request->order)->get();
        
        for ($i=0; $i < $count; $i++) { 
            OrderDetails::where('id', $request->order[$i])->update([
                'id_menu' => $request->id_menu[$i],
                'namamenu' => $request->namamenu[$i],
                'harga' => $request->harga[$i],
                'qty' => $request->qty[$i],
                'total' => $request->harga[$i] * $request->qty[$i],
            ]);
            // $od = $request->order[$i];
            // $od->id_order = $mesen->id;
            // $od->id_meja = $request->id_meja;
            // $od->id_menu = $request->id_menu[$i];
            // $od->namamenu = $request->namamenu[$i];
            // $od->harga = $request->harga[$i];
            // $od->qty = $request->qty[$i];
            // $od->total = $request->harga[$i] * $request->qty[$i];
            // $od->save();
        }
        // dd($od->total);

        return redirect('/dashboard/order')->with('success', 'Menu Berhasil Diperbaharui!');
    }

}
