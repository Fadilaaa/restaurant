<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // protected $table = "order";

    public function menu(){
        return $this->hasMany(ListMenu::class, 'id', 'id_menu');
    }

    public function listmeja(){
        return $this->belongsTo(ListMeja::class, 'id', 'id_meja');
    }

    public function user(){
        return $this->belongsTo(User::class, 'id', 'nip');
    }

    public function jenismenu(){
        return $this->hasMany(JenisMenu::class);
    }

    public function orderdetails(){
        return $this->hasOne(OrderDetails::class, 'id', 'id_order');
    }
}
