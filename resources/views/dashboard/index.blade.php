@extends('dashboard.layouts.main')

@section('container')
    
<div class="row mt-4">
    <div class="col-12 col-sm-4 col-xl-3 mb-4">
        <div class="card border-0 shadow">
            <div class="card-body">
                <div class="row d-block d-xl-flex align-items-center">
                    <div class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                        <div class="icon-shape icon-shape-primary rounded me-4 me-sm-0">
                            <svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>
                        </div>
                        <div class="d-sm-none">
                            <h2 class="h5"><a href="?order">Order</a></h2>
                            <h3 class="fw-extrabold mb-1">{{ $order->count() }}</h3>
                        </div>
                    </div>
                    <div class="col-12 col-xl-7 px-xl-0">
                        <div class="d-none d-sm-block">
                            <h2 class="h6 text-gray-400 mb-0"><a href="?order">Order</a></h2>
                            <h3 class="fw-extrabold mb-2">{{ $order->count() }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4 col-xl-3 mb-4">
        <div class="card border-0 shadow">
            <div class="card-body">
                <div class="row d-block d-xl-flex align-items-center">
                    <div class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                        <div class="icon-shape icon-shape-secondary rounded me-4 me-sm-0">
                            <svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z" clip-rule="evenodd"></path></svg>
                        </div>
                        <div class="d-sm-none">
                            <h2 class="fw-extrabold h5"><a href="?orderdetails">Order Details</a></h2>
                            <h3 class="mb-1">{{ $orderdetails->count() }}</h3>
                        </div>
                    </div>
                    <div class="col-12 col-xl-7 px-xl-0">
                        <div class="d-none d-sm-block">
                            <h2 class="h6 text-gray-400 mb-0"><a href="?orderdetails">Order Details</a></h2>
                            <h3 class="fw-extrabold mb-2">{{ $orderdetails->count() }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4 col-xl-3 mb-4">
        <div class="card border-0 shadow">
            <div class="card-body">
                <div class="row d-block d-xl-flex align-items-center">
                    <div class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                        <div class="icon-shape icon-shape-tertiary rounded me-4 me-sm-0">
                            <svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 3a1 1 0 000 2v8a2 2 0 002 2h2.586l-1.293 1.293a1 1 0 101.414 1.414L10 15.414l2.293 2.293a1 1 0 001.414-1.414L12.414 15H15a2 2 0 002-2V5a1 1 0 100-2H3zm11.707 4.707a1 1 0 00-1.414-1.414L10 9.586 8.707 8.293a1 1 0 00-1.414 0l-2 2a1 1 0 101.414 1.414L8 10.414l1.293 1.293a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
                        </div>
                        <div class="d-sm-none">
                            <h2 class="fw-extrabold h5"><a href="?listmenu">List Menu</a></h2>
                            <h3 class="mb-1">{{ $listmenu->count() }}</h3>
                        </div>
                    </div>
                    <div class="col-12 col-xl-7 px-xl-0">
                        <div class="d-none d-sm-block">
                            <h2 class="h6 text-gray-400 mb-0"><a href="?listmenu">List Menu</a></h2>
                            <h3 class="fw-extrabold mb-2">{{ $listmenu->count() }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($_GET["order"]))
<div class="card m-3 p-4">
    <div class="row">
        <div class="col-12">
            <table id="example" class="table table-centered table-nowrap mb-0 rounded" width="100%">
                <thead class="text-center">
                    <th width="5px">No</th>
                    <th>Nomor Meja</th>
                    <th>Atas Nama</th>
                    <th>Status</th>
                </thead>
                <tbody>
                @foreach($order as $o)
                    <tr class="text-center">
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $o->id_meja }}</td>
                        <td>{{ $o->atasnama }}</td>
                        <td>{{ $o->status }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(isset($_GET["orderdetails"]))
<div class="card m-3 p-4">
    <div class="row">
        <div class="col-12">
            <table id="example" class="table table-centered table-nowrap mb-0 rounded" width="100%">
                <thead class="text-center">
                    <th width="5px">No</th>
                    <th>Nomor Order</th>
                    <th>Nomor Meja</th>
                    <th>ID Menu</th>
                    <th>Nama Menu</th>
                    <th>Harga</th>
                    <th>Qty</th>
                </thead>
                <tbody>
                @foreach($orderdetails as $o)
                    <tr class="text-center">
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $o->id_order }}</td>
                        <td>{{ $o->id_meja }}</td>
                        <td>{{ $o->id_menu }}</td>
                        <td>{{ $o->namamenu }}</td>
                        <td>{{ $o->harga }}</td>
                        <td>{{ $o->qty }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(isset($_GET["listmenu"]))
<div class="card m-3 p-4">
    <div class="row">
        <div class="col-12">
            <table id="example" class="table table-centered table-nowrap mb-0 rounded" width="100%">
                <thead class="text-center">
                    <th width="5px">No</th>
                    <th>Kode Menu</th>
                    <th>Nama Menu</th>
                    <th>Jenis Menu</th>
                    <th>Harga</th>
                </thead>
                <tbody>
                @foreach($listmenu as $menu)
                    <tr class="text-center">
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $menu->kodemenu }}</td>
                        <td>{{ $menu->namamenu }}</td>
                        <td>{{ $menu->jenismenu }}</td>
                        <td>{{ $menu->harga }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    
@endisset

@endsection