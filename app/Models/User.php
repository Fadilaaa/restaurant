<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

     /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role)
    {
        // check param $role dengan field role
        if ($role == $this->role){
            return true;
        }
        return false;
    }

    public function listmeja(){
        return $this->hasMany(ListMeja::class);
    }

    public function menu(){
        return $this->hasMany(ListMenu::class);
    }

    public function order(){
        return $this->hasMany(Order::class, 'id', 'user_id');
    }

    public function jenismenu(){
        return $this->hasMany(JenisMenu::class);
    }

    public function orderdetails(){
        return $this->hasMany(OrderDetails::class, 'id', 'id_order');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'nip');
    }

    public function karyawan(){
        return $this->belongsTo(Karyawan::class, 'nip', 'nip');
    }
}
