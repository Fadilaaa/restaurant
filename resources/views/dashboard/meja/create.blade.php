@extends('dashboard.layouts.main')

@section('container')
<div class="mb-3 mb-lg-0">
    <h1 class="h4">List Meja</h1>
    
</div>

                    <div class="row">
                        <div class="col-12 mb-4">
                            <div class="card-border-0 shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-body">
                                                @if(session()->has('success'))
                                                <div class="alert alert-success col-lg-12" role="alert">
                                                  {{ session('success') }}
                                                </div>
                                                @endif
                                                <form action="/dashboard/meja/create" method="get">
                                                    @csrf
                                                <div class="input-group mb-3">
                                                    <select name="meja" id="" class="form-select" style="max-width: 200px">
                                                        @if(request('meja'))
                                                        <option value="{{ request('meja') }}" select hidden>Status Meja</option>
                                                            <option value="0">Meja Kosong</option>
                                                            <option value="1">Meja Dipesan</option>
                                                        @else
                                                            <option value="0">Meja Kosong</option>
                                                            <option value="1">Meja Dipesan</option>
                                                        @endif
                                                    </select>
                                                    <button class="btn btn-info"><i class="bi bi-search"></i></button>
                                                </div>
                                            </form>
                                            @if(!$_GET)
                                            @else
                                                @if($_GET['meja'] == 0)
                                                <div class="alert alert-success col-lg-12">Menampilkan Meja Kosong</div>
                                                @elseif($_GET['meja'] == 1)
                                                <div class="alert alert-success col-lg-12">Menampilkan Meja Yang Sedang Dipesan</div>
                                                @endif
                                            @endif
                                            <br>
                                                <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                    <div class="col-lg-12">  
                                                        <table id="example" class="table table-centered table-nowrap mb-0 rounded " style="width=100%">
                                                            <thead class="text-center text-dark">
                                                                <tr class="text-center">
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Kode Meja</th>
                                                                    <th scope="col">Nama Meja</th>
                                                                    <th scope="col">Status</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($listmeja as $meja)
                                                            <tr class="text-center">
                                                                <td>{{ $loop->iteration }}</td> 
                                                                <td>{{ $meja->kodemeja }}</td>
                                                                <td>{{ $meja->namameja }}</td>
                                                                <td>{{ $meja->status }}</td>
                                                                <td>
                                                                    <form action="/dashboard/meja/create" method="get">
                                                                        @csrf
                                                                    <div class="input-group mb-3 align-text-center">
                                                                            @if(request('meja'))
                                                                                        <input type="hidden" name="meja" value="0">
                                                                                        <button type="button" class="btn btn-block col-8 btn-gray-800 mb-3 align-text-center" data-bs-toggle="modal" disabled data-bs-target="#modal-form-signup{{ $meja->id }}" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg>
                                                                                        </button>
                                                                            @else
                                                                                    <input type="hidden" name="meja" value="1">
                                                                                    <button type="button" class="btn btn-block col-8 btn-gray-800 mb-3 align-text-center" data-bs-toggle="modal" data-bs-target="#modal-form-signup{{ $meja->id }}" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                                        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg>
                                                                                    </button>
                                                                            @endif
                                                                    </div>
                                                                </form>
                                                        <!-- Modal Content -->
                                                        <input type="hidden" id="{{ $meja->id }}" name="{{ $meja->id }}" value="{{ $meja->id }}">
                                                        <div class="modal fade" id="modal-form-signup{{ $meja->id }}" name="modal-form-signup{{ $meja->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup{{ $meja->id }}" aria-hidden="true" value="{{ $meja->id }}">
                                                            <div class="modal-dialog modal-dialog-centered" role="document" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                <div class="modal-content">
                                                                    <div class="modal-body p-0">
                                                                        <div class="card p-3 p-lg-4">
                                                                            <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                            <div class="text-center text-md-center mb-4 mt-md-0">
                                                                                <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                            </div>
                                                                            <form action="{{ route('pesan') }}" class="mt-4" method="post" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                                @method('post')
                                                                                @csrf
                                                                                <input type="hidden" class="form-control" id="id_meja" name="id_meja" value="{{ $meja->id }}">
                                                                                <div class="mb-3">
                                                                                    <input type="hidden" class="form-control" name="id_meja" value="{{ $meja->id }}">
                                                                                    <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                    <input type="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                                  @error('atasnama')
                                                                                    <div class="invalid-feedback">
                                                                                      {{ $message }}
                                                                                    </div>
                                                                                  @enderror
                                                                                  </div>
                                                                                {{-- @endforeach --}}
                                                                                @foreach($listmenu as $menu)
                                                                                <!-- Form -->
                                                                                <div class="form-group mb-4">
                                                                                    <div class="input-group">
                                                                                          <div class="card">
                                                                                            <div class="row">
                                                                                                <div class="col-sm-6">
                                                                                                    <input type="hidden" value="{{ $menu->id }}">
                                                                                                    @if($menu->gambar)
                                                                                                    <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-12" style='height: 100%; width: 100%; object-fit: contain'>
                                                                                                @else
                                                                                                    <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                                @endif
                                                                                                </div>
                                                                                                <div class="col-5">
                                                                                                    <h4 class="card-title mt-2" name="namamenu[]" value="{{ $menu->namamenu }}">{{ $menu->namamenu }}</h4>
                                                                                                    <label for="deskripsi" class="form-label" value="{{ $menu->deskripsi }}">{{ $menu->deskripsi }}</label>
                                                                                                    <br>
                                                                                                    {{-- <label for="harga" class="form-label">{{ $menu->harga }}</label> --}}
                                                                                                    <div class="mb-3 col-lg-12">
                                                                                                        <input type="hidden" name="id_menu[]" value="{{ $menu->id }}">
                                                                                                        <input type="hidden" name="namamenu[]" class="form-control @error('namamenu') is-invalid @enderror" value="{{ $menu->namamenu }}">
                                                                                                        <input type="text" name="harga[]" class="form-control @error('harga') is-invalid @enderror" value="{{ $menu->harga }}">
                                                                                                        <br>
                                                                                                        <input type="number" min="0" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty[]"  value="0">
                                                                                                      @error('qty')
                                                                                                        <div class="invalid-feedback">
                                                                                                          {{ $message }}
                                                                                                        </div>
                                                                                                      @enderror
                                                                                                      </div>
                                                                                                </div>
                                                                                            </div>
                                                                                          </div>
                                                                                    </div>  
                                                                                </div>
                                                                                @endforeach
                                                                                <!-- End of Form -->
                                                                                <div class="d-grid">
                                                                                    <button type="submit" class="btn btn-warning">Tambah</button>
                                                                                </div>
                                                                            </form>
                                                                        
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End of Modal Content -->
                                                        @endforeach
                                                    </td>
                                                    {{-- @endforeach  --}}
                                                </tbody>
                                            </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- <div class="col-4">
                                            <div class="card-body">
                                                <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                    <div class="col-lg-6">
                                                        <!-- Button Modal -->
                                                        @if($sudah_memesan != 1)
                                                        <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup2"><span><svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg></span></button>
                                                    @elseif($sudah_memesan == 1)
                                                        <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup2"><span><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg></span></button>
                                                        <div class="alert alert-warning">
                                                            Sudah Dipesan
                                                        </div>
                                                    @endif
                                                        <h5 class="text-center">Meja 2</h5>
                                                        <!-- Modal Content -->
                                                        <div class="modal fade" id="modal-form-signup2" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup2" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-body p-0">
                                                                        <div class="card p-3 p-lg-4">
                                                                            <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                            <div class="text-center text-md-center mb-4 mt-md-0">
                                                                                <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                            </div>
                                                                            <form action="/dashboard/meja/create" class="mt-4">
                                                                                @csrf
                                                                                <div class="mb-3">
                                                                                    <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                    <input tyatasnama="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                                    @error('atasnama')
                                                                                    <div class="invalid-feedback">
                                                                                        {{ $message }}
                                                                                    </div>
                                                                                    @enderror
                                                                                </div>
                                                                                <!-- Form -->
                                                                                @foreach($listmenu as $menu)
                                                                                <div class="form-group mb-4">
                                                                                    <div class="input-group">
                                                                                          <div class="card">
                                                                                            <div class="row">
                                                                                                <div class="col-md-4">
                                                                                                    @if($menu->gambar)
                                                                                                    <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-sm-12 d-block">
                                                                                                @else
                                                                                                    <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                                @endif
                                                                                                </div>
                                                                                                <div class="col-md-8">
                                                                                                    <h4 class="card-title mt-2">{{ $menu->namamenu }}</h4>
                                                                                                    <label for="deskripsi" class="form-label">{{ $menu->deskripsi }}</label>
                                                                                                    <br>
                                                                                                    <label for="harga" class="form-label">{{ $menu->harga }}</label>
                                                                                                    <div class="mb-3 col-7">
                                                                                                        <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty" required value="{{ old('qty') }}">
                                                                                                      @error('qty')
                                                                                                        <div class="invalid-feedback">
                                                                                                          {{ $message }}
                                                                                                        </div>
                                                                                                      @enderror
                                                                                                      </div>
                                                                                                </div>
                                                                                            </div>
                                                                                          </div>
                                                                                    </div>  
                                                                                </div>
                                                                                @endforeach
                                                                                <!-- End of Form -->
                                                                                <div class="d-grid">
                                                                                    <button type="submit" class="btn btn-warning">Pesan</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End of Modal Content -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-4">
                                            <div class="card-body">
                                                <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                    <div class="col-lg-6">
                                                        <!-- Button Modal -->
                                                        @if($sudah_memesan != 1)
                                                        <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup3"><span><svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg></span></button>
                                                    @elseif($sudah_memesan == 1)
                                                        <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup3"><span><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg></span></button>
                                                        <div class="alert alert-warning">
                                                            Sudah Dipesan
                                                        </div>
                                                    @endif
                                                        <h5 class="text-center">Meja 3</h5>
                                                        <!-- Modal Content -->
                                                        <div class="modal fade" id="modal-form-signup3" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup3" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-body p-0">
                                                                        <div class="card p-3 p-lg-4">
                                                                            <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                            <div class="text-center text-md-center mb-4 mt-md-0">
                                                                                <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                            </div>
                                                                            <form action="/dashboard/meja/create" class="mt-4">
                                                                                @csrf
                                                                                <div class="mb-3">
                                                                                    <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                    <input tyatasnama="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                                  @error('atasnama')
                                                                                    <div class="invalid-feedback">
                                                                                      {{ $message }}
                                                                                    </div>
                                                                                  @enderror
                                                                                  </div>
                                                                                @foreach($listmenu as $menu)
                                                                                <!-- Form -->
                                                                                <div class="form-group mb-4">
                                                                                    <div class="input-group">
                                                                                          <div class="card">
                                                                                            <div class="row">
                                                                                                <div class="col-md-4">
                                                                                                    @if($menu->gambar)
                                                                                                    <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-sm-12 d-block">
                                                                                                @else
                                                                                                    <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                                @endif
                                                                                                </div>
                                                                                                <div class="col-md-8">
                                                                                                    <h4 class="card-title mt-2">{{ $menu->namamenu }}</h4>
                                                                                                    <label for="deskripsi" class="form-label">{{ $menu->deskripsi }}</label>
                                                                                                    <br>
                                                                                                    <label for="harga" class="form-label">{{ $menu->harga }}</label>
                                                                                                    <div class="mb-3 col-7">
                                                                                                        <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty" required value="{{ old('qty') }}">
                                                                                                      @error('qty')
                                                                                                        <div class="invalid-feedback">
                                                                                                          {{ $message }}
                                                                                                        </div>
                                                                                                      @enderror
                                                                                                      </div>
                                                                                                </div>
                                                                                            </div>
                                                                                          </div>
                                                                                    </div>  
                                                                                </div>
                                                                                @endforeach
                                                                                <!-- End of Form -->
                                                                                <div class="d-grid">
                                                                                    <button type="submit" class="btn btn-warning">Pesan</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End of Modal Content -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>   

                                <div class="row ">
                                    <div class="col-4">
                                        <div class="card-body">
                                            <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                <div class="col-lg-6">
                                                    <!-- Button Modal -->
                                                    @if($sudah_memesan != 1)
                                                    <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup4"><span><svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg></span></button>
                                                @elseif($sudah_memesan == 1)
                                                    <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup4"><span><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg></span></button>
                                                    <div class="alert alert-warning">
                                                        Sudah Dipesan
                                                    </div>
                                                @endif
                                                    <h5 class="text-center">Meja 4</h5>
                                                    <!-- Modal Content -->
                                                    <div class="modal fade" id="modal-form-signup4" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup4" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body p-0">
                                                                    <div class="card p-3 p-lg-4">
                                                                        <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        <div class="text-center text-md-center mb-4 mt-md-0">
                                                                            <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                        </div>
                                                                        <form action="/dashboard/meja/create" class="mt-4">
                                                                            @csrf
                                                                            <div class="mb-3">
                                                                                <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                <input tyatasnama="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                              @error('atasnama')
                                                                                <div class="invalid-feedback">
                                                                                  {{ $message }}
                                                                                </div>
                                                                              @enderror
                                                                              </div>
                                                                            @foreach($listmenu as $menu)
                                                                            <!-- Form -->
                                                                            <div class="form-group mb-4">
                                                                                <div class="input-group">
                                                                                      <div class="card">
                                                                                        <div class="row">
                                                                                            <div class="col-md-4">
                                                                                                @if($menu->gambar)
                                                                                                <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-sm-12 d-block">
                                                                                            @else
                                                                                                <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                            @endif
                                                                                            </div>
                                                                                            <div class="col-md-8">
                                                                                                <h4 class="card-title mt-2">{{ $menu->namamenu }}</h4>
                                                                                                <label for="deskripsi" class="form-label">{{ $menu->deskripsi }}</label>
                                                                                                <br>
                                                                                                <label for="harga" class="form-label">{{ $menu->harga }}</label>
                                                                                                <div class="mb-3 col-7">
                                                                                                    <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty" required value="{{ old('qty') }}">
                                                                                                  @error('qty')
                                                                                                    <div class="invalid-feedback">
                                                                                                      {{ $message }}
                                                                                                    </div>
                                                                                                  @enderror
                                                                                                  </div>
                                                                                            </div>
                                                                                        </div>
                                                                                      </div>
                                                                                </div>  
                                                                            </div>
                                                                            @endforeach
                                                                            <!-- End of Form -->
                                                                            <div class="d-grid">
                                                                                <button type="submit" class="btn btn-warning">Pesan</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Modal Content -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="card-body">
                                            <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                <div class="col-lg-6">
                                                    <!-- Button Modal -->
                                                    @if($sudah_memesan != 1)
                                                    <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup5"><span><svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg></span></button>
                                                @elseif($sudah_memesan == 1)
                                                    <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup5"><span><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg></span></button>
                                                    <div class="alert alert-warning">
                                                        Sudah Dipesan
                                                    </div>
                                                @endif
                                                    <h5 class="text-center">Meja 5</h5>
                                                    <!-- Modal Content -->
                                                    <div class="modal fade" id="modal-form-signup5" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup5" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body p-0">
                                                                    <div class="card p-3 p-lg-4">
                                                                        <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        <div class="text-center text-md-center mb-4 mt-md-0">
                                                                            <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                        </div>
                                                                        <form action="/dashboard/meja/create" class="mt-4">
                                                                            @csrf
                                                                            <div class="mb-3">
                                                                                <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                <input tyatasnama="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                              @error('atasnama')
                                                                                <div class="invalid-feedback">
                                                                                  {{ $message }}
                                                                                </div>
                                                                              @enderror
                                                                              </div>
                                                                            @foreach($listmenu as $menu)
                                                                            <!-- Form -->
                                                                            <div class="form-group mb-4">
                                                                                <div class="input-group">
                                                                                      <div class="card">
                                                                                        <div class="row">
                                                                                            <div class="col-md-4">
                                                                                                @if($menu->gambar)
                                                                                                <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-sm-12 d-block">
                                                                                            @else
                                                                                                <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                            @endif
                                                                                            </div>
                                                                                            <div class="col-md-8">
                                                                                                <h4 class="card-title mt-2">{{ $menu->namamenu }}</h4>
                                                                                                <label for="deskripsi" class="form-label">{{ $menu->deskripsi }}</label>
                                                                                                <br>
                                                                                                <label for="harga" class="form-label">{{ $menu->harga }}</label>
                                                                                                <div class="mb-3 col-7">
                                                                                                    <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty" required value="{{ old('qty') }}">
                                                                                                  @error('qty')
                                                                                                    <div class="invalid-feedback">
                                                                                                      {{ $message }}
                                                                                                    </div>
                                                                                                  @enderror
                                                                                                  </div>
                                                                                            </div>
                                                                                        </div>
                                                                                      </div>
                                                                                </div>  
                                                                            </div>
                                                                            @endforeach
                                                                            <!-- End of Form -->
                                                                            <div class="d-grid">
                                                                                <button type="submit" class="btn btn-warning">Pesan</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Modal Content -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="card-body">
                                            <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                <div class="col-lg-6">
                                                    <!-- Button Modal -->
                                                    @if($sudah_memesan != 1)
                                                    <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup6"><span><svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg></span></button>
                                                @elseif($sudah_memesan == 1)
                                                    <button type="button" class="btn btn-block col-12 btn-gray-800 mb-3" data-bs-toggle="modal" data-bs-target="#modal-form-signup6"><span><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg></span></button>
                                                    <div class="alert alert-warning">
                                                        Sudah Dipesan
                                                    </div>
                                                @endif
                                                    <h5 class="text-center">Meja 6</h5>
                                                    <!-- Modal Content -->
                                                    <div class="modal fade" id="modal-form-signup6" tabindex="-1" role="dialog" aria-labelledby="modal-form-signu6" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body p-0">
                                                                    <div class="card p-3 p-lg-4">
                                                                        <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        <div class="text-center text-md-center mb-4 mt-md-0">
                                                                            <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                        </div>
                                                                        <form action="/dashboard/meja/create" class="mt-4">
                                                                            @csrf
                                                                            <div class="mb-3">
                                                                                <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                <input tyatasnama="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                              @error('atasnama')
                                                                                <div class="invalid-feedback">
                                                                                  {{ $message }}
                                                                                </div>
                                                                              @enderror
                                                                              </div>
                                                                            @foreach($listmenu as $menu)
                                                                            <!-- Form -->
                                                                            <div class="form-group mb-4">
                                                                                <div class="input-group">
                                                                                      <div class="card">
                                                                                        <div class="row">
                                                                                            <div class="col-md-4">
                                                                                                @if($menu->gambar)
                                                                                                <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-sm-12 d-block">
                                                                                            @else
                                                                                                <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                            @endif
                                                                                            </div>
                                                                                            <div class="col-md-8">
                                                                                                <h4 class="card-title mt-2">{{ $menu->namamenu }}</h4>
                                                                                                <label for="deskripsi" class="form-label">{{ $menu->deskripsi }}</label>
                                                                                                <br>
                                                                                                <label for="harga" class="form-label">{{ $menu->harga }}</label>
                                                                                                <div class="mb-3 col-7">
                                                                                                    <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty" required value="{{ old('qty') }}">
                                                                                                  @error('qty')
                                                                                                    <div class="invalid-feedback">
                                                                                                      {{ $message }}
                                                                                                    </div>
                                                                                                  @enderror
                                                                                                  </div>
                                                                                            </div>
                                                                                        </div>
                                                                                      </div>
                                                                                </div>  
                                                                            </div>
                                                                            @endforeach
                                                                            <!-- End of Form -->
                                                                            <div class="d-grid">
                                                                                <button type="submit" class="btn btn-warning">Pesan</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Modal Content -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>     --}}
                        </div>
                    </div>
                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
@endsection