@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row">
                <h6 class="font-weight text-dark mt-2">List Menu</h6>
            </div>
        </div>
        <div class="row-border">
            <div class="col-lg-12">
                <div class="card-body">
                    <div class="dashboard">
                        <div class="container-fluid">             
                <div class="app-card-body px-4 w-100 my-3">
                    <div class="col-lg-12 border-right">
                                <div class="text-end">
                                    <button class="btn btn-warning profile-button" type="button"><a href="/dashboard/order/{{ $order->id }}/edit" class="text-decoration-none text-dark"><i class="bi bi-pencil-square"></i></a></button>
                                </div>
                                    <div class="col-md-12"><label class="labels">Meja   :</label> {{ $order->id_meja }}</div>
                                    <div class="col-md-12"><label class="labels">Atas Nama   : </label> {{ $order->atasnama }}</div>
                                    <div class="col-md-12"><label class="labels">Kode Order   : </label>{{ $order->kodeorder }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 border-right">
                            <table class="table table-centered table-nowrap mb-0 rounded " style="width=100%">
                                <thead class="text-center text-dark">
                                    <tr class="text-center">
                                        <th scope="col">#</th>
                                        <th scope="col">Nomor Meja</th>
                                        <th scope="col">Nomor Menu</th>
                                        <th scope="col">Nama Menu</th>
                                        <th scope="col">Qty</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Total</th>
                                    </tr>
                            </thead>
                            <tbody>
                                @foreach ($orderdetails as $orderdetail)
                                <tr class="text-center">
                                    <td>{{ $loop->iteration }}</td> 
                                    <td>{{ $orderdetail->id_meja }}</td>
                                    <td>{{ $orderdetail->id_menu }}</td>
                                    <td>{{ $orderdetail->namamenu }}</td>
                                    <td>{{ $orderdetail->qty }}</td>
                                    <td>{{ $orderdetail->harga }}</td>
                                    <td>{{ $orderdetail->total }}</td>
                                </tbody>
                                @endforeach
                                <tfoot>
                                    <td>TOTAL</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ $total }}</td>
                                </tfoot>
                        </div>
                    </div>
                </div>
</div>
        </div>
    </div>          
</div>
</div>
        </div>
    
<script>
    function edit(){
        $('#kodemenu').removeAttr('disabled');
        $('#namamenu').removeAttr('disabled');
        $('#jenismenu').removeAttr('disabled');
        $('#deskripsi').removeAttr('disabled');
        $('#harga').removeAttr('disabled');
        $('#gambar').removeAttr('disabled');
    }

    function previewImage(){
        const gambar = document.querySelector('#gambar');
        const imgPreview = document.querySelector('.img-preview1');

        imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(gambar.files[0]);

        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
@endsection