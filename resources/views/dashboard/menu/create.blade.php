@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight text-dark">Create Menu</h6>
        </div>
        <div class="row-border">
            <div class="col-lg-12">
                <div class="card-body">
                    <div class="dashboard">
                        <div class="container-fluid">             
                <div class="app-card-body px-4 w-100 my-3">
                <form action="/dashboard/menu" method="post" enctype="multipart/form-data">
                    {{-- @method('put') --}}
                    @csrf
                      <div class="mb-3">
                        <label for="kodemenu" class="form-label">Kode Menu</label>
                        <input type="text" class="form-control @error('kodemenu') is-invalid @enderror" id="kodemenu" name="kodemenu" required value="{{ old('kodemenu') }}">
                      @error('kodemenu')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                      </div>
                      <div class="mb-3">
                        <label for="namamenu" class="form-label">Nama Menu</label>
                        <input type="text" class="form-control @error('namamenu') is-invalid @enderror" id="namamenu" name="namamenu" required value="{{ old('namamenu') }}">
                      @error('namamenu')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                      </div>
                      <div class="mb-3">
                        <label for="jenismenu" class="form-label">Jenis Menu</label>
                        <select name="jenismenu" id="jenismenu" class="form-select">
                            <option value="makanan">Makanan</option>
                            <option value="minuman">Minuman</option>
                        </select>
                      @error('jenismenu')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                      </div>
                      <div class="mb-3">
                        <label for="deskripsi" class="form-label">Deskripsi</label>
                        <input type="text" class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" name="deskripsi" required value="{{ old('deskripsi') }}">
                      @error('deskripsi')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                      </div>
                      <div class="mb-3">
                        <label for="harga" class="form-label">Harga</label>
                        <input type="number" min="0" class="form-control @error('harga') is-invalid @enderror" id="harga" name="harga" required value="{{ old('harga') }}">
                      @error('harga')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                      </div>
                      <div>
                        <label for="gambar" class="form-label">Gambar</label>
                        <img class="img-preview1 img-fluid mb-3 col-sm-5 d-block">
                        <input type="file" name="gambar" id="gambar" class="form-control @error('gambar') is-invalid @enderror" value="{{ old('gambar') }}" onchange="previewImage()">
                        @error('gambar')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                      <div class="text-end mt-4">
                        <button type="submit" class="btn btn-primary">Create Menu</button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
        </div>    
    </div>
</div>
</div>
</div>
<script>
    function previewImage(){
        const gambar = document.querySelector('#gambar');
        const imgPreview = document.querySelector('.img-preview1');

        imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(gambar.files[0]);

        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
@endsection