<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user(){
        $this->hasOne(Karyawan::class, 'nip', 'nip');
    }

    public function menu(){
        return $this->hasMany(ListMenu::class);
    }

    public function listmeja(){
        return $this->hasMany(ListMeja::class);
    }

    public function orderdetails(){
        return $this->hasMany(OrderDetails::class);
    }
}
