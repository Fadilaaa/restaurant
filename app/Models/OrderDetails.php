<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function menu(){
        return $this->hasMany(ListMenu::class, 'id', 'id_menu');
    }

    public function listmeja(){
        return $this->belongsTo(ListMeja::class, 'id', 'id_meja');
    }

    public function user(){
        return $this->hasMany(User::class);
    }

    public function jenismenu(){
        return $this->hasMany(JenisMenu::class);
    }

    public function order(){
        return $this->belongsTo(Order::class, 'id', 'id_order');
    }
}
