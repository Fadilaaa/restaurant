<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderDetails>
 */
class OrderDetailsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_order' => $this->faker->numberBetween($min=1, $max=2),
            'id_meja' => $this->faker->numberBetween($min=1, $max=2),
            'id_menu' => $this->faker->numberBetween($min=1, $max=2),
            'namamenu' => $this->faker->sentence(1, 2),
            'harga' => $this->faker->numberBetween($min=4, $max=5),
            'qty' => $this->faker->numberBetween($min=1, $max=2),
            'total' => $this->faker->numberBetween($min=1, $max=2),
        ];
    }
}
