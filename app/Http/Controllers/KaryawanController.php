<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.profile.index', [
            'title' => 'profile'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if($request["password"] != $request["password2"]){
            return redirect('/dashboard/profile/' . auth()->user()->id)->with('error', 'Password Tidak Sama!');
        }

        User::where('id', $request["id"])->
        update(['password' => bcrypt($request["password"])]);
        return redirect('/dashboard/profile')->with('success', 'Password Berhasil Diubah!');

    }

    public function update_profil(Request $request, User $user, Karyawan $karyawan)
    {
            $kwn = [
            'nip'=> 'required',
            'name'=> 'required',
            'email'=> 'required',
        ];

        $validateData = $request->validate($kwn);

        if($request->file('profile')){
            if($request->oldImage){
                Storage::delete($request->oldImage);
            }
            $validateData['profile'] = $request->file('profile')->store('/profile');
        }

        // dd($request);
        Karyawan::where('id', $karyawan->id)
        ->update($validateData);

        return redirect('/dashboard/profile')->with('success', 'Profil berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
