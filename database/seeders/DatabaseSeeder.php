<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\ListMeja;
use App\Models\ListMenu;
use App\Models\Order;
use App\Models\JenisMenu;
use App\Models\Karyawan;
use App\Models\OrderDetails;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    User::create([
        'nip' => '111',
        'username' => 'pelayan',
        'password' => bcrypt('pelayan'),
        'role' => 'pelayan',
    ]);

    User::create([
        'nip' => '222',
        'username' => 'kasir',
        'password' => bcrypt('kasir'),
        'role' => 'kasir',
    ]);

    Karyawan::create([
        'nip' => '111',
        'name' => 'Fadila',
        'email' => 'fadilakbar79@gmail.com'
    ]);

    Karyawan::create([
        'nip' => '222',
        'name' => 'Akbar',
        'email' => 'fadila.akbar@raharja.info'
    ]);

    JenisMenu::create([
        'jmenu' => 'Makanan',
    ]);

    JenisMenu::create([
        'jmenu' => 'Minuman',
    ]);

    // ListMeja::factory(1)->create();

    ListMeja::create([
        'kodemeja' => '1',
        'namameja' => 'Meja 1',
        'status' => 0
    ]);

    ListMeja::create([
        'kodemeja' => '2',
        'namameja' => 'Meja 2',
        'status' => 0
    ]);

    ListMeja::create([
        'kodemeja' => '3',
        'namameja' => 'Meja 3',
        'status' => 0
    ]);

    ListMeja::create([
        'kodemeja' => '4',
        'namameja' => 'Meja 4',
        'status' => 0
    ]);

    ListMeja::create([
        'kodemeja' => '5',
        'namameja' => 'Meja 5',
        'status' => 0
    ]);

    ListMeja::create([
        'kodemeja' => '6',
        'namameja' => 'Meja 6',
        'status' => 0
    ]);
 
    OrderDetails::factory(3)->create();

    ListMenu::factory(1)->create();
 
    Order::factory(1)->create();

    }
}
