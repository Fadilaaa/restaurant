<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderDetailsController;
use App\Http\Controllers\ListMenuController;
use App\Http\Controllers\ListMejaController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\LoginController;
use App\Models\Order;
use App\Models\ListMenu;
use App\Models\OrderDetails;
use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Auth;
use Laravel\Ui\AuthRouteMethods;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function (){
//     return view('welcome');
// });
Route::get('/', [OrderDetailsController::class, 'index']);

Route::get('/dashboard', function(){
    return view('dashboard.index', [
        'title' => 'Dashboard',
        'order' => Order::all(),
        'orderdetails' => OrderDetails::all(),
        'listmenu' => ListMenu::all(),
    ]);
})->middleware('auth');

Route::get('login', [LoginController::class, 'index']);
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [LoginController::class, 'register'])->middleware('guest');
Route::post('/register', [LoginController::class, 'store']);

Route::get('/dashboard/profile', [KaryawanController::class, 'index'])->middleware('auth');
Route::put('/dashboard/profile/{karyawan}', [KaryawanController::class, 'update_profil']);
Route::resource('/dashboard/profile', KaryawanController::class);

Route::get('/dashboard/menu', [ListMenuController::class, 'index'])->middleware('auth');
// Route::get('/dashboard/menu/create', [ListMenuController::class, 'create']);
// Route::get('/dashboard/menu/{menu}/edit', [ListMenuController::class, 'edit']);
Route::post('/dashboard/menu/{menu}', [ListMenuController::class, 'update']);
Route::resource('/dashboard/menu', ListMenuController::class);

// Route::get('/dashboard/meja', [ListMejaController::class, 'index1']);
Route::get('/dashboard/meja/create', [ListMejaController::class, 'create'])->middleware('auth');
Route::post('/dashboard/meja/create', [ListMejaController::class, 'store'])->name('pesan');
Route::resource('/dashboard/meja', ListMejaController::class);

Route::get('/dashboard/order', [OrderController::class, 'index'])->middleware('auth');
Route::post('/dashboard/order/{order}', [OrderController::class, 'update'])->name('mesen');
Route::put('/dashboard/order/{id}/close', [OrderController::class, 'close']);
Route::get('/dashboard/order/{id}/edit', [OrderController::class, 'editorder']);
Route::put('/dashboard/order/{id}', [OrderController::class, 'updateorder']);
Route::get('/dashboard/order/show/{id}', [OrderController::class, 'show']);
Route::resource('/dashboard/order', OrderController::class);



// Route::get('/dashboard/modal/meja1', [ListMejaController::class, 'index01']);

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
