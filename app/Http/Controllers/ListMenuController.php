<?php

namespace App\Http\Controllers;

use App\Models\JenisMenu;
use App\Models\ListMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ListMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListMenu $listmenu)
    {
        $listmenu = ListMenu::all();
        return view('dashboard.menu.index', [
            'title' => 'listmenu',
            'listmenu' => $listmenu
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'kodemenu' => 'required',
            'namamenu' => 'required',
            'jenismenu' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
        ];

        $validateData = $request->validate($rules);

        if($request->file('gambar')){
            $validateData['gambar'] = $request->file('gambar')->store('gambar');
        }

        ListMenu::create($validateData);

        return redirect('/dashboard/menu')->with('success', 'Data Barang Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListMenu  $listMenu
     * @return \Illuminate\Http\Response
     */
    public function show(ListMenu $listMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListMenu  $listMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(ListMenu $menu)
    {
        return view('dashboard.menu.edit',[
            'menu' => $menu,
            'jenismenu' => JenisMenu::all()
            // 'jenismenu' => JenisMenu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ListMenu  $listMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListMenu $menu)
    {
        $update = [
            'kodemenu' => 'required',
            'namamenu' => 'required',
            'jenismenu' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
        ];
        
        // $validatedData['user_id'] = auth()->user()->id;
        $validatedData = $request->validate($update);
        
        if($request->file('gambar')){
            if($request->oldImage){
                Storage::delete($request->oldImage);
            }
            $validatedData['gambar'] = $request->file('gambar')->store('gambar');
        }
        
        ListMenu::where('id', $menu->id)
        ->update($validatedData);
        // dd($validatedData);
        
        // $validateData['user_id'] = auth()->user()->id;
        return redirect('/dashboard/menu')->with('success', 'Anda Berhasil Memperbaharui Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListMenu  $listMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListMenu $menu)
    {
        ListMenu::destroy($menu->id);
        return redirect('/dashboard/menu')->with('success', 'Menu Berhasil Dihapus');
    }
}
