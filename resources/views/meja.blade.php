<!--

=========================================================
* Volt Pro - Premium Bootstrap 5 Dashboard
=========================================================

* Product Page: https://themesberg.com/product/admin-dashboard/volt-bootstrap-5-dashboard
* Copyright 2021 Themesberg (https://www.themesberg.com)
* License (https://themesberg.com/licensing)

* Designed and coded by https://themesberg.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. Please contact us to request a removal.

-->
<!DOCTYPE html>
<html lang="en">

<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Primary Meta Tags -->
<title>Volt Premium Bootstrap Dashboard - Sign up page</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="title" content="Volt Premium Bootstrap Dashboard - Sign up page">
<meta name="author" content="Themesberg">
<meta name="description" content="Volt Pro is a Premium Bootstrap 5 Admin Dashboard featuring over 800 components, 10+ plugins and 20 example pages using Vanilla JS.">
<meta name="keywords" content="bootstrap 5, bootstrap, bootstrap 5 admin dashboard, bootstrap 5 dashboard, bootstrap 5 charts, bootstrap 5 calendar, bootstrap 5 datepicker, bootstrap 5 tables, bootstrap 5 datatable, vanilla js datatable, themesberg, themesberg dashboard, themesberg admin dashboard" />
<link rel="canonical" href="https://themesberg.com/product/admin-dashboard/volt-premium-bootstrap-5-dashboard">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://demo.themesberg.com/volt-pro">
<meta property="og:title" content="Volt Premium Bootstrap Dashboard - Sign up page">
<meta property="og:description" content="Volt Pro is a Premium Bootstrap 5 Admin Dashboard featuring over 800 components, 10+ plugins and 20 example pages using Vanilla JS.">
<meta property="og:image" content="https://themesberg.s3.us-east-2.amazonaws.com/public/products/volt-pro-bootstrap-5-dashboard/volt-pro-preview.jpg">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://demo.themesberg.com/volt-pro">
<meta property="twitter:title" content="Volt Premium Bootstrap Dashboard - Sign up page">
<meta property="twitter:description" content="Volt Pro is a Premium Bootstrap 5 Admin Dashboard featuring over 800 components, 10+ plugins and 20 example pages using Vanilla JS.">
<meta property="twitter:image" content="https://themesberg.s3.us-east-2.amazonaws.com/public/products/volt-pro-bootstrap-5-dashboard/volt-pro-preview.jpg">

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="120x120" href="/assets/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="/assets/img/favicon/site.webmanifest">
<link rel="mask-icon" href="/assets/img/favicon/safari-pinned-tab.svg" color="#ffffff">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<!-- Sweet Alert -->
<link type="text/css" href="/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">

<!-- Notyf -->
<link type="text/css" href="/vendor/notyf/notyf.min.css" rel="stylesheet">

<!-- Volt CSS -->
<link type="text/css" href="/css/volt.css" rel="stylesheet">

<!-- NOTICE: You can use the _analytics.html partial to include production code specific code & trackers -->

</head>

<body>

    <!-- NOTICE: You can use the _analytics.html partial to include production code specific code & trackers -->
    

    <main>

        <!-- Section -->
        {{-- <section class="vh-lg-100 mt-5 mt-lg-2 bg-soft d-flex align-items-center"> --}}
            {{-- <div class="container"> --}}
                {{-- <div class="row justify-content-center form-bg-image" data-background-lg="/assets/img/illustrations/signin.svg"> --}}
                    {{-- <div class="col-12 d-flex align-items-center justify-content-center"> --}}
                        <div class="bg-white shadow border-0 rounded border-light p-4 p-lg-5 w-100 ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-body">
                        <div class="mb-3 mb-lg-0">
                            <h1 class="h4">List Meja</h1>
                        </div>
                    <div class="row">
                        <div class="col-12 mb-4">
                            <div class="card-border-0 shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-body">
                                                <div class="text-end">
                                                    <a href="/login" class="btn btn-primary">Login</a>
                                                </div>
                        
                                                @if(session()->has('success'))
                                                <div class="alert alert-success col-lg-12" role="alert">
                                                  {{ session('success') }}
                                                </div>
                                                @endif
                                                <form action="/" method="get">
                                                    @csrf
                                                <div class="input-group mb-3">
                                                    <select name="meja" id="" class="form-select" style="max-width: 200px">
                                                        @if(request('meja'))
                                                        <option value="{{ request('meja') }}" select hidden>Status Meja</option>
                                                            <option value="0">Meja Kosong</option>
                                                            <option value="1">Meja Dipesan</option>
                                                        @else
                                                            <option value="0">Meja Kosong</option>
                                                            <option value="1">Meja Dipesan</option>
                                                        @endif
                                                    </select>
                                                    <button class="btn btn-info"><i class="bi bi-search"></i></button>
                                                </div>
                                            </form>
                                            @if(!$_GET)
                                            @else
                                                @if($_GET['meja'] == 0)
                                                <div class="alert alert-success col-lg-12">Menampilkan Meja Kosong</div>
                                                @elseif($_GET['meja'] == 1)
                                                <div class="alert alert-success col-lg-12">Menampilkan Meja Yang Sedang Dipesan</div>
                                                @endif
                                            @endif
                                            <br>
                                                <div class="row d-block d-xl-flex col-lg-12 align-items-center">
                                                    <div class="col-lg-12">  
                                                        <table id="example" class="table table-centered table-nowrap mb-0 rounded " style="width=100%">
                                                            <thead class="text-center text-dark">
                                                                <tr class="text-center">
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Kode Meja</th>
                                                                    <th scope="col">Nama Meja</th>
                                                                    <th scope="col">Status</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($listmeja as $meja)
                                                            <tr class="text-center">
                                                                <td>{{ $loop->iteration }}</td> 
                                                                <td>{{ $meja->kodemeja }}</td>
                                                                <td>{{ $meja->namameja }}</td>
                                                                <td>{{ $meja->status }}</td>
                                                                <td>
                                                                    <form action="/meja" method="get">
                                                                        @csrf
                                                                    <div class="input-group mb-3 align-text-center">
                                                                            @if(request('meja'))
                                                                                        <input type="hidden" name="meja" value="0">
                                                                                        <button type="button" class="btn btn-block col-8 btn-gray-800 mb-3 align-text-center" disabled data-bs-toggle="modal" disabled data-bs-target="#modal-form-signup{{ $meja->id }}" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path></svg>
                                                                                        </button>
                                                                            @else
                                                                                    <input type="hidden" name="meja" value="1">
                                                                                    <button type="button" class="btn btn-block col-8 btn-gray-800 mb-3 align-text-center" disabled data-bs-toggle="modal" data-bs-target="#modal-form-signup{{ $meja->id }}" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                                        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg>
                                                                                    </button>
                                                                            @endif
                                                                    </div>
                                                                </form>
                                                        <!-- Modal Content -->
                                                        <input type="hidden" id="{{ $meja->id }}" name="{{ $meja->id }}" value="{{ $meja->id }}">
                                                        <div class="modal fade" id="modal-form-signup{{ $meja->id }}" name="modal-form-signup{{ $meja->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup{{ $meja->id }}" aria-hidden="true" value="{{ $meja->id }}">
                                                            <div class="modal-dialog modal-dialog-centered" role="document" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                <div class="modal-content">
                                                                    <div class="modal-body p-0">
                                                                        <div class="card p-3 p-lg-4">
                                                                            <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                            <div class="text-center text-md-center mb-4 mt-md-0">
                                                                                <h1 class="mb-0 h4">Pesan Menu </h1>
                                                                            </div>
                                                                            <form action="{{ route('pesan') }}" class="mt-4" method="post" id="{{ $meja->id }}" value="{{ $meja->id }}">
                                                                                @method('post')
                                                                                @csrf
                                                                                <input type="hidden" class="form-control" id="id_meja" name="id_meja" value="{{ $meja->id }}">
                                                                                <div class="mb-3">
                                                                                    <input type="hidden" class="form-control" name="id_meja" value="{{ $meja->id }}">
                                                                                    <label for="atasnama" class="form-label">Atas Nama</label>
                                                                                    <input type="text" class="form-control @error('atasnama') is-invalid @enderror" id="atasnama" name="atasnama" required value="{{ old('atasnama') }}">
                                                                                  @error('atasnama')
                                                                                    <div class="invalid-feedback">
                                                                                      {{ $message }}
                                                                                    </div>
                                                                                  @enderror
                                                                                  </div>
                                                                                {{-- @endforeach --}}
                                                                                @foreach($listmenu as $menu)
                                                                                <!-- Form -->
                                                                                <div class="form-group mb-4">
                                                                                    <div class="input-group">
                                                                                          <div class="card">
                                                                                            <div class="row">
                                                                                                <div class="col-4">
                                                                                                    <input type="hidden" value="{{ $menu->id }}">
                                                                                                    @if($menu->gambar)
                                                                                                    <img src="{{ asset('/storage/' . $menu->gambar) }}" class="img-preview card-img-left col-12 d-block">
                                                                                                @else
                                                                                                    <img class="rounded-circle mt-5" width="100px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                                                                                @endif
                                                                                                </div>
                                                                                                <div class="col-md-8">
                                                                                                    <h4 class="card-title mt-2" name="namamenu[]" value="{{ $menu->namamenu }}">{{ $menu->namamenu }}</h4>
                                                                                                    <label for="deskripsi" class="form-label" value="{{ $menu->deskripsi }}">{{ $menu->deskripsi }}</label>
                                                                                                    <br>
                                                                                                    {{-- <label for="harga" class="form-label">{{ $menu->harga }}</label> --}}
                                                                                                    <div class="mb-3 col-12">
                                                                                                        <input type="hidden" name="id_menu[]" value="{{ $menu->id }}">
                                                                                                        <input type="hidden" name="namamenu[]" class="form-control @error('namamenu') is-invalid @enderror" value="{{ $menu->namamenu }}">
                                                                                                        <input type="text" name="harga[]" class="form-control @error('harga') is-invalid @enderror" value="{{ $menu->harga }}">
                                                                                                        <input type="number" min="0" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty[]"  value="0">
                                                                                                      @error('qty')
                                                                                                        <div class="invalid-feedback">
                                                                                                          {{ $message }}
                                                                                                        </div>
                                                                                                      @enderror
                                                                                                      </div>
                                                                                                </div>
                                                                                            </div>
                                                                                          </div>
                                                                                    </div>  
                                                                                </div>
                                                                                @endforeach
                                                                                <!-- End of Form -->
                                                                                <div class="d-grid">
                                                                                    <button type="submit" class="btn btn-warning">Tambah</button>
                                                                                </div>
                                                                            </form>
                                                                        
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End of Modal Content -->
                                                        @endforeach
                                                    </td>
                                                    {{-- @endforeach  --}}
                                                </tbody>
                                            </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <!-- Core -->
<script src="/vendor/@popperjs/core/dist/umd/popper.min.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Vendor JS -->
<script src="/vendor/onscreen/dist/on-screen.umd.min.js"></script>

<!-- Slider -->
<script src="/vendor/nouislider/distribute/nouislider.min.js"></script>

<!-- Smooth scroll -->
<script src="/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>

<!-- Charts -->
<script src="/vendor/chartist/dist/chartist.min.js"></script>
<script src="/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>

<!-- Datepicker -->
<script src="/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<!-- Sweet Alerts 2 -->
<script src="/vendor/sweetalert2/dist/sweetalert2.all.min.js"></script>

<!-- Moment JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>

<!-- Vanilla JS Datepicker -->
<script src="/vendor/vanillajs-datepicker/dist/js/datepicker.min.js"></script>

<!-- Notyf -->
<script src="/vendor/notyf/notyf.min.js"></script>

<!-- Simplebar -->
<script src="/vendor/simplebar/dist/simplebar.min.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!-- Volt JS -->
<script src="/assets/js/volt.js"></script>

    
</body>

</html>
