@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight text-dark">Edit Menu</h6>
        </div>
        <div class="row-border">
            <div class="col-12">
                <div class="card-body">
                    <div class="dashboard">
                        <div class="container-fluid">             
                <div class="app-card-body px-4">
                    <div class="text-center text-md-center mb-4">
                        <h1 class="mb-0 h4">Pesanan</h1>
                    </div>
                    <form action="/dashboard/order/{{ $order->id }}" class="mt-4" method="post" >
                        @method('put')
                        @csrf
                        <input type="hidden" class="form-control" name="id" value="{{ $order->id }}">
                            <label for="Nomor Meja" class="form-label text-left">Nomor Meja</label>
                                <input type="text" class="form-control" name="id_meja" value="{{ $order->id_meja }}">
                            <label for="atasnama" class="form-label text-left">Atas Nama</label>
                                <input type="text" class="form-control @error('atasnama') is-invalid @enderror" disabled id="atasnama" name="atasnama" required value="{{ old('atasnama', $order->atasnama) }}">
                            @error('atasnama')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                      <br>
                    @foreach($orderdetails as $od)
                    {{-- @var_dump($menu->id) --}}
                    <!-- Form -->
                        <div class="form-group mb-4">
                            <div class="input-group">
                                <div class="col-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-12">
                                                    <div class="mb-2">
                                                    <div class="text-center">
                                                            <input type="hidden" name="order[]" value="{{ $od->id }}">
                                                            <input type="hidden" name="id_menu[]" value="{{ $od->id_menu }}">
                                                            <h4 class="card-title mt-3" value="{{ $od->namamenu }}">{{ $od->namamenu }}</h4>
                                                    </div>
                                                    <br>
                                                    <div class="mb-3">
                                                            <input type="hidden" name="namamenu[]" class="form-control @error('namamenu') is-invalid @enderror col-10" value="{{ $od->namamenu }}">
                                                            <input type="text" name="harga[]" class="form-control @error('harga') is-invalid @enderror col-10" value="{{ $od->harga }}">
                                                            <br>
                                                            <input type="number" min="0" class="form-control @error('qty') is-invalid @enderror col-10" id="qty[]" name="qty[]" value="{{ old('qty', $od->qty) }}">
                                                            @error('qty')
                                                            <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            </div>
                        </div>
                        @endforeach
                        <!-- End of Form -->
                        <div class="d-grid">
                            <button type="submit" class="btn btn-warning">Tambah</button>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>    
    </div>
</div>
</div>
</div>
<script>
    function previewImage(){
        const gambar = document.querySelector('#gambar');
        const imgPreview = document.querySelector('.img-preview1');

        imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(gambar.files[0]);

        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
@endsection