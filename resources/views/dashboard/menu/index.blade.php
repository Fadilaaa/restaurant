@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">
    <div class="mb-3 mb-lg-0">
        <h1 class="h4">List Menu</h1>
    </div>
    <!-- Page Heading -->
    {{-- <div class="card shadow mb-4"> --}}
      
        <div class="card">
        <div class="card-body">
            <div class="dashboard">
                <div class="container-fluid">
                    @if(session()->has('success'))
              <div class="alert alert-success col-lg-8" role="alert">
                {{ session('success') }}
              </div>
              @endif
            {{-- <form action="/barang" method="get">
                @csrf
            <div class="input-group">
                <select name="barangmasuk" id="barangmasuk" class="form-select">
                    @if(request('barangmasuk'))
                    <option value="{{ request('barangmasuk') }}" select hidden>Barang Masuk{{ request('barangmasuk') }}</option>
                        <option value="1">Barang Masuk 1</option>
                        <option value="2">Barang Masuk 2</option>
                        <option value="3">Barang Masuk 3</option>
                        <option value="4">Barang Masuk 4</option>
                    @else
                        <option value="1">Barang Masuk 1</option>
                        <option value="2">Barang Masuk 2</option>
                        <option value="3">Barang Masuk 3</option>
                        <option value="4">Barang Masuk 4</option>
                    @endif
                </select>
                <button class="btn btn-info"><i class="fa fa-search"></i></button>
            </div>
        </form>
        @if(!$_GET)
        @else
            @if($_GET['barangmasuk'] == 1)
            <div class="alert alert-success col-lg-12">Menampilkan Halaman Barang Masuk Minggu Pertama</div>
            @elseif($_GET['barangmasuk'] == 2)
            <div class="alert alert-success col-lg-12">Menampilkan Halaman Barang Masuk Minggu Kedua</div>
            @elseif($_GET['barangmasuk'] == 3)
            <div class="alert alert-success col-lg-12">Menampilkan Halaman Barang Masuk Minggu Ketiga</div>
            @elseif($_GET['barangmasuk'] == 4)
            <div class="alert alert-success col-lg-12">Menampilkan Halaman Barang Masuk Minggu Keempat</div>
            @endif
        @endif --}}
            {{-- <a href="/barang/create" class="btn btn-primary mb-3">Create barang</a> --}}
            <table id="example" class="table table-centered table-nowrap mb-0 rounded " style="width=100%">
                <thead class="text-center text-dark">
                    <tr class="text-center">
                        <th scope="col">#</th>
                        <th scope="col">Kode Menu</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Action</th>
                    </tr>
            </thead>
            <tbody>
                <a href="/dashboard/menu/create" class="btn btn-primary mb-3">Create Menu</a>
                @foreach ($listmenu as $menu)
                <tr class="text-center">
                    <td>{{ $loop->iteration }}</td> 
                    <td>{{ $menu->kodemenu }}</td>
                    <td>{{ $menu->namamenu }}</td>
                    <td>{{ $menu->jenismenu }}</td>
                    <td>{{ $menu->harga }}</td>
                    <td>
                        <a href="/dashboard/menu/{{ $menu->id }}/edit" class="btn btn-warning">Edit</a>
                        <form action="/dashboard/menu/{{ $menu->id }}" method="post" class="d-inline">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin?')"><span data-feather="x-circle"></span>Delete</button>
                        </form>
                    </td>
                @endforeach 
            </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
@endsection