@extends('dashboard.layouts.main')

@section('container')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight text-dark">List Menu</h6>
        </div>
        <div class="row-border">
            <div class="col-lg-12">
                <div class="card-body">
                    <div class="dashboard">
                        <div class="container-fluid">             
                <div class="app-card-body px-4 w-100 my-3">
                <form action="/dashboard/menu/{{ $menu->id }}" method="post" enctype="multipart/form-data">
                    {{-- @method('put') --}}
                    @csrf
                    <div class="row">
                <div class="col-md-6 border-right">
                    <div class="p-3 py-5">
                        <div class="row">
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-3">
                      <label for="kodemenu" class="col-form-label">Kode Menu</label>
                    </div>
                    <div class="col-9">
                      <input type="text" id="kodemenu" name="kodemenu" class="form-control" value="{{ $menu->kodemenu }}" disabled>
                    </div>
                </div>
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-3">
                      <label for="namamenu" class="col-form-label">Nama Menu</label>
                    </div>
                    <div class="col-9">
                      <input type="text" id="namamenu" name="namamenu" class="form-control" value="{{ $menu->namamenu }}" disabled>
                    </div>
                </div>
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-3">
                      <label for="jenismenu" class="col-form-label">Jenis Menu</label>
                    </div>
                    <div class="col-9">
                        <select oninput="this.className='form-control'" class="form-select @error('jenismenu') is-invalid @enderror" id="jenismenu" name="jenismenu" required autofocus value="{{ old('jenismenu', $menu->jenismenu) }}" disabled>
                            @if(old('jenismenu', $menu->jenismenu) == $menu->jenismenu)
                            <option value="{{ $menu->jenismenu }}" selected hidden>{{ $menu->jenismenu }}</option>
                        @endif
                            <option value="makanan">Makanan</option>
                            <option value="minuman">Minuman</option>
                        </select> 
                        @error('jenismenu')
                            <div class="invalid-feedback">
                            {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-3">
                      <label for="deskripsi" class="col-form-label">Deskripsi</label>
                    </div>
                    <div class="col-9">
                      <input type="text" id="deskripsi" name="deskripsi" class="form-control" value="{{ $menu->deskripsi }}" disabled>
                    </div>
                </div>
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-3">
                      <label for="harga" class="col-form-label">Harga</label>
                    </div>
                    <div class="col-9">
                      <input type="text" id="harga" name="harga" class="form-control" value="{{ $menu->harga }}" disabled>
                    </div>
                </div>
            </div>
        </div>
                </div>
                    <div class="col-md-6 ">
                        <div class="p-3 py-5">    
                            <div class="row-mt-2">
                                <label for="gambar" class="form-label">Foto :</label>
                                <input type="file" class="form-control @error('gambar') is-invalid @enderror mb-3" id="gambar" name="gambar" onchange="previewImage()" >
                                @error('gambar')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                                <input type="hidden" name="oldImage" value="{{ $menu->gambar }}">
                                @if($menu->gambar)
                                    <img src="{{ asset('storage/' . $menu->gambar) }}" class="img-preview img-fluid mb-3 col-12 d-block">
                                @else
                                    <img class="img-preview1 img-fluid mb-3 col-sm-12 d-block" >
                                @endif
                            </div>
                        </div>
                    </div>
            </div>
    <div class="text-end mb-3">
        <button class="btn btn-warning" onclick="edit()" type="button">Edit</button>
        <button class="btn btn-info" type="submit">Save</button>
    </div>
</form>
</div>
        </div>
    </div>          
</div>
</div>
        </div>
    
<script>
    function edit(){
        $('#kodemenu').removeAttr('disabled');
        $('#namamenu').removeAttr('disabled');
        $('#jenismenu').removeAttr('disabled');
        $('#deskripsi').removeAttr('disabled');
        $('#harga').removeAttr('disabled');
        $('#gambar').removeAttr('disabled');
    }

    function previewImage(){
        const gambar = document.querySelector('#gambar');
        const imgPreview = document.querySelector('.img-preview1');

        imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(gambar.files[0]);

        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>
@endsection