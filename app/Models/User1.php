<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User1 extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    // protected $fillable = [
    //     'nip',
    //     'name',
    //     'rola',
    //     'username',
    //     'password',
    //     'profile',
    //     'email',
    // ];

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role)
    {
        // check param $role dengan field role
        if ($role == $this->role){
            return true;
        }
        return false;
    }

    public function listmeja(){
        return $this->hasMany(ListMeja::class);
    }

    public function menu(){
        return $this->hasMany(ListMenu::class);
    }

    public function order(){
        return $this->hasMany(Order::class, 'id', 'user_id');
    }

    public function jenismenu(){
        return $this->hasMany(JenisMenu::class);
    }

    public function orderdetails(){
        return $this->hasMany(OrderDetails::class, 'id', 'id_order');
    }
}
